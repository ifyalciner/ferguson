// MIT License
//
// Copyright (c) 2022 Ibrahim Faruk YALCINER
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

#ifndef FERGUSON_INCLUDE_THREADS_ACTION_H
#define FERGUSON_INCLUDE_THREADS_ACTION_H

/* Local */
#include <ferguson/synchronizers/anytime.h>
#include <ferguson/utils.h>

/* Boost */
#include <boost/property_tree/json_parser.hpp>
#include <boost/property_tree/ptree.hpp>
#include <functional>
#include <type_traits>
#include <utility>

/* 3rd party*/
#include <ferguson/iterator_tlp.h>
#include <ferguson/threads/task.h>

namespace Ferguson::this_action {
inline void
sleepUntil(TimePoint const &deadline) {
  boost::this_thread::sleep_until(deadline);
}
inline void
sleepFor(TimeUnit const &duration) {
  boost::this_thread::sleep_for(duration);
}
}// namespace Ferguson::this_action

namespace Ferguson::Threads {

using ActionIDType = uint64_t;
class ActionID {
public:
  explicit ActionID(ActionIDType id): id(id) {}
  const ActionIDType id;
};

/**********************************************************************************************************************/

// @note: C++17 noexcept is part of function type, somehow it prevents Callback being fully defined.
// Had to remove noexcept specifier from Callback type.
// @see: https://stackoverflow.com/questions/38760784/how-will-c17-exception-specifier-type-system-work
using ActionProductCB = std::function<void(std::any)>;

/**********************************************************************************************************************/

/**
 * @brief Synchronizer for any Action instance
 */
class ActionBase {
  NOCOPYASSIGN(ActionBase);

public:
  /**
   * @brief Returns the root id for the actions without parent
   */
  static ActionID const &
  root() {
    static ActionID id(0);
    return id;
  }

  /**
   * @brief
   */
  ActionBase() = default;

  /**
   * @brief Return current action's ID.
   */
  [[nodiscard]] virtual ActionIDType
  id() const = 0;

  /**
   * @brief Returns current action's parent's ID.
   */
  [[nodiscard]] virtual ActionIDType
  parent() const = 0;

  /**
   * @brief Terminates action
   */
  virtual void
  preempt() = 0;

  /**
   * @brief Informs if an action is completed
   * @return Will return true if terminate is requested (not terminated) or result is generated.
   */
  virtual bool
  finished() = 0;

  virtual void waitUntilFinished(TimeUnit) = 0;
};
DEFINE_SMARTPTR(ActionBase);

class Action: public virtual ActionBase {
public:
  /**
   * @brief Return product if there is one
   * @return Pair of (is retrieving successful, retrieved product pointer)
   *         * Retrieving can fail in case of:
   *            * Terminate has been requested
   *            * Result is generated (no more product is available after result is generated)
   *            * Callback method is provided and products are not cached by Action
   *         * Product can be nullptr if there isn't any available at the time of request
   */
  virtual std::any
  getProduct() = 0;

  /**
   * @brief Throws exception if there is any
   */
  virtual void
  throwException() = 0;
};

DEFINE_SMARTPTR(Action);

/**********************************************************************************************************************/

/**
 * @brief Wrapper class for product and return type handling
 */
template<typename ProductType>
class ActionTypeWrapper {

private:
  ActionSPtr const action;

public:
  explicit ActionTypeWrapper(ActionSPtr action): action(std::move(action)) {}

  /**
   * @brief Return current action's ID.
   */
  [[nodiscard]] ActionIDType
  id() const {
    return action->id();
  }

  /**
   * @brief Returns current action's parent's ID.
   */
  [[nodiscard]] ActionIDType
  parent() const {
    return action->parent();
  }

  /**
   * @brief Terminates action
   */
  void
  preempt() {
    action->preempt();
  }

  /**
   * @brief Informs if an action is completed
   * @return Will return true if terminate is requested (not terminated) or result is generated.
   */
  [[nodiscard]] bool
  finished() {
    return action->finished();
  }

  /**
     * @brief Wait until action is finished
     * @param timeout
     */
  void
  waitUntilFinished(TimeUnit timeout) {
    action->waitUntilFinished(timeout);
  }

  void
  waitUntilFinished() {
    action->waitUntilFinished(TimeUnit::max());
  }

  /**
   * @brief Return product if there is one
   * @return Optional ProductType object
   *         * Retrieving can fail in case of:
   *            * Terminate has been requested
   *            * Result is generated (no more product is available after result is generated)
   *            * Callback method is provided and products are not cached by Action
   *         * Product can be nullptr if there isn't any available at the time of request
   */
  std::optional<ProductType>
  getProduct() {
    auto any_product = action->getProduct();
    if(any_product.has_value()) {
      return std::any_cast<ProductType>(any_product);
    }
    return std::nullopt;
  }

  /**
     * @brief Throws exception if there is any
     */
  void
  throwException() {
    action->throwException();
  }

  struct ActionIterator {
  private:
    std::optional<ProductType> next_fb;

  public:
    void
    next(ActionTypeWrapper<ProductType> *ref) {
      // Can't fail since there is no condition, except termination which result doesn't matter at that point.
      next_fb = ref->getProduct();
    }

    void
    begin(ActionTypeWrapper<ProductType> *ref) {
      // Can't fail since there is no condition, except termination which result doesn't matter at that point.
      next_fb = ref->getProduct();
    }

    void
    end([[maybe_unused]] const ActionTypeWrapper<ProductType> *ref) {
      next_fb = std::nullopt;
    }

    ProductType &
    get([[maybe_unused]] ActionTypeWrapper<ProductType> *ref) {
      return next_fb.value();
    }

    ProductType const &
    get([[maybe_unused]] ActionTypeWrapper<ProductType> const *ref) {
      return next_fb.value();
    }

    [[nodiscard]] bool
    equals(const ActionIterator &s) const {
      return next_fb == s.next_fb;
    }
  };

  // Mutable Iterator. No point having explicit const iterator since returned values are shared_ptr of const type.
  using iterator = iterator_tpl::iterator<ActionTypeWrapper<ProductType>,//
                                          ProductType const &,           //
                                          ActionIterator>;

  iterator
  begin() {
    return iterator::begin(this);
  }

  iterator
  end() {
    return iterator::end(this);
  }
};

/**********************************************************************************************************************/

template<typename ProductType>
ActionTypeWrapper<ProductType>
make_action(std::shared_ptr<Task<ProductType>> task_ptr,//
            ActionID parent,
            std::string const &instance_name,   //
            TimeUnit const &max_terminate_delay,//
            identity_t<std::optional<std::function<void(ProductType)>>> product_cb) {

  std::optional<ActionProductCB> product_cb_type_fix;
  if(product_cb.has_value()) {
    product_cb_type_fix = [product_cb = std::move(product_cb.value())](std::any product) {
      product_cb(std::any_cast<ProductType>(product));
    };
  }
  // Forward declaration
  ActionSPtr factory(ActionID parent,                    //
                     TaskBaseSPtr t,                     //
                     std::string const &instance_name,   //
                     std::string const &task_class_name, //
                     TimeUnit const &max_terminate_delay,//
                     std::optional<ActionProductCB> const &product_cb);
  return ActionTypeWrapper<ProductType>(factory(parent,                                       //
                                                std::dynamic_pointer_cast<TaskBase>(task_ptr),//
                                                instance_name,                                //
                                                task_ptr->className(),                        //
                                                max_terminate_delay,                          //
                                                product_cb_type_fix));
}

// Enable if T is derived from TypedTask
template<typename T,                                    //
         typename ProductType = typename T::ProductType,//
         typename             = std::enable_if_t<std::is_base_of_v<Task<ProductType>, T>>>
ActionTypeWrapper<ProductType>
make_action(std::shared_ptr<T> task_ptr,        //
            ActionID parent,                    //
            std::string const &instance_name,   //
            TimeUnit const &max_terminate_delay,//
            identity_t<std::optional<std::function<void(typename T::ProductType)>>> const &product_cb = std::nullopt) {
  return make_action(std::dynamic_pointer_cast<Task<typename T::ProductType>>(task_ptr),//
                     parent,                                                            //
                     instance_name,                                                     //
                     max_terminate_delay,                                               //
                     product_cb);
}

}// namespace Ferguson::Threads

#endif//FERGUSON_INCLUDE_THREADS_ACTION_H
