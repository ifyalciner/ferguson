// MIT License
//
// Copyright (c) 2022 Ibrahim Faruk YALCINER
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

#ifndef YIELDER_H_
#define YIELDER_H_

/* 3rd Paty */
// https://www.boost.org/doc/libs/1_53_0/libs/coroutine/doc/html/coroutine/coroutine.html
#ifdef DT_BOOST_COROUTINE2
#include <boost/coroutine2/all.hpp>
namespace coroutines = boost::coroutines2;
#endif

#ifdef DT_BOOST_COROUTINE
#include <boost/coroutine/all.hpp>
namespace coroutines = boost::coroutines;
#endif

/* Local */
#include <ferguson/synchronizers/experimental/communicator.h>

namespace Ferguson::Yielder {

/******************************************************************************/
/* Host class */
using Host               = Communicator::Host;
using ReturnCallbackType = Communicator::ReturnCallbackType;

DEFINE_SMARTPTR(Host);

/******************************************************************************/
/* Thread class */
class Thread: public virtual Communicator::Thread {
public:
  /*** DEFINITIONS ***/
  using Coroutine = coroutines::coroutine<Communicator::OutputBaseCSPtr>;

  /*** METHODS ***/
  // Copying not allowed
  Thread()                = default;
  Thread(const Thread &)  = delete;
  Thread(const Thread &&) = delete;
  void
  operator=(const Thread &) = delete;
  void
  operator=(const Thread &&) = delete;

  using SPtr  = std::shared_ptr<Thread>;
  using CSPtr = std::shared_ptr<Thread const>;
  using UPtr  = std::unique_ptr<Thread>;
  using CUPtr = std::unique_ptr<Thread const>;
};

DEFINE_SMARTPTR(Thread);

std::pair<HostSPtr, ThreadSPtr>
factory(
    TimeUnit response_time                    = TimeUnit(TimeUnit::max()),//
    ReturnCallbackType const &return_callback = [](const Communicator::OutputBaseCSPtr &) {});

/**
 * @brief Design template for thread run method. It can be used directly with
 * or can be used as a base for proper thread method
 * @param sync Synchronization object
 * @param procedure Procedure to be executed
 */
[[maybe_unused]] static void
run(Thread::SPtr sync,                              //
    std::function<void(Communicator::InputBaseCSPtr,//
                       Thread::Coroutine::push_type &)>
        procedure) {
  auto parent_procedure = [&]() {
    Communicator::InputBaseCSPtr data;
    bool received;
    while(true) {
      std::tie(received, data) = sync->fromHost();
      if(received) {
        // Create new coroutine
        Thread::Coroutine::pull_type co_procedure(
            [procedure, data](auto &&PH1) { procedure(data, std::forward<decltype(PH1)>(PH1)); });

        for(const auto &out : co_procedure) {
          if(sync->isTerminateRequested()) {
            return;
          }
          sync->toHost(out);
        }
      }
      if(sync->isTerminateRequested()) {
        return;
      }
    }
  };

  Interruptable::run(sync, parent_procedure);
}

}// namespace Ferguson::Yielder

#endif// YIELDER_H_
