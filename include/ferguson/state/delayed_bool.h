// MIT License
//
// Copyright (c) 2022 Ibrahim Faruk YALCINER
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef FERGUSON_INCLUDE_FERGUSON_STATE_DELAYED_BOOL_H
#define FERGUSON_INCLUDE_FERGUSON_STATE_DELAYED_BOOL_H

#include <memory>

#include <ferguson/time.h>
#include <ferguson/utils.h>

namespace Ferguson::State {

/**
 * @brief A boolean handing delays and jitters for its state
 */
class DelayedBool {
public:
  /**
   * @brief
   * @param initial      : Initial value
   * @param true_delay   : For a value to be set to true DelayedBool should not be set to false during this delay.
   *                       Jitters `false` sets are removed automatically except the ones appearing in the time frame
   *                       [true_delay-activation_delay,true_delay]. During this time a flag cannot be identified as activation_delay
   *                       since a cancelling `true` value can be set at anytime between
   *                       [true_delay,true_delay+activation_delay) depending on when cancelling value appears.
    * @param false_delay : For a value to be set to true DelayedBool should not be set to false during this delay.
    *                      Jitters `true` sets are removed automatically except the ones appearing in the time frame
    *                      [true_delay-activation_delay,true_delay]. During this time a flag cannot be identified as activation_delay
    *                      since a cancelling `false` value can be set at anytime between
    *                      [true_delay,true_delay+activation_delay) depending on when cancelling value appears.
   * @param activation_delay : It is modelled as another DelayedBool where setting a value to true or false requires non
   *                       changing state for given activation_delay duration.
   * @return
   */
  static std::shared_ptr<DelayedBool>
  factory(bool initial                        = false,                //
          Ferguson::TimeUnit true_delay       = Ferguson::TimeUnit(0),//
          Ferguson::TimeUnit false_delay      = Ferguson::TimeUnit(0),//
          Ferguson::TimeUnit activation_delay = Ferguson::TimeUnit(0));

  /**
   * @brief  Set boolean value to x, it will get into effect after it's assigned delay value is passed
   * @param x
   * @return Current boolean value. It is possible that you assign false and this method returns true.
   */
  virtual DelayedBool&
  operator=(bool x) = 0;

  /**
   * @brief Get current value
   * @return Current boolean value
   */
  virtual
  operator bool() const = 0;

  /**
   * @brief Set boolean value immediately bypassing any delay mechanism. This will remove all delayed values from being
   *            set when their delay time is over.
   * @return Current boolean value. Which should be equal to x.
   */
  virtual bool
  setImmediately(bool x) = 0;
};
}// namespace Ferguson::State

#endif//FERGUSON_INCLUDE_FERGUSON_STATE_DELAYED_BOOL_H
