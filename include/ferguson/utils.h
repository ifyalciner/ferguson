// MIT License
//
// Copyright (c) 2022 Ibrahim Faruk YALCINER
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

#ifndef FERGUSON_INCLUDE_FERGUSON_H
#define FERGUSON_INCLUDE_FERGUSON_H

#include <functional>
#include <memory>
#include <optional>

namespace Ferguson {

#define DEFINE_SMARTPTR(className)                                                                                     \
  using className##SPtr  = std::shared_ptr<className>;                                                                 \
  using className##CSPtr = std::shared_ptr<className const>;                                                           \
  using className##UPtr  = std::unique_ptr<className>;                                                                 \
  using className##CUPtr = std::unique_ptr<className const>;                                                           \
  using className##WPtr  = std::weak_ptr<className>;                                                                   \
  using className##CWPtr = std::weak_ptr<className const>;

/* Basic boolean function */
using NullaryBoolF       = std::function<bool()>;
static auto ALWAYS_FALSE = NullaryBoolF([]() { return false; });
static auto ALWAYS_TRUE  = NullaryBoolF([]() { return true; });

#define NOCOPYASSIGN(className)                                                                                        \
private:                                                                                                               \
  className(const className &)       = delete;                                                                         \
  className(const className &&)      = delete;                                                                         \
  void operator=(const className &)  = delete;                                                                         \
  void operator=(const className &&) = delete;

template<class T,//
         class B,//
         class = std::enable_if<std::is_base_of<B, T>::value>>
std::optional<std::shared_ptr<T>>
as(std::optional<std::shared_ptr<B>> const &o) {
  return o ? std::make_optional(std::static_pointer_cast<T>(o.value())) : std::nullopt;
}

}// namespace Ferguson

#endif//FERGUSON_INCLUDE_FERGUSON_H
