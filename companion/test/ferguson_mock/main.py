#  MIT License
#
#  Copyright (c) 2023 Ibrahim Faruk YALCINER
#
#  Permission is hereby granted, free of charge, to any person obtaining a copy
#  of this software and associated documentation files (the "Software"), to deal
#  in the Software without restriction, including without limitation the rights
#  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#  copies of the Software, and to permit persons to whom the Software is
#  furnished to do so, subject to the following conditions:
#
#  The above copyright notice and this permission notice shall be included in all
#  copies or substantial portions of the Software.
#
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE

import dataclasses
import json
import random
import socket
import struct
import threading
import time
import typing

import lorem


# Press the green button in the gutter to run the script.

@dataclasses.dataclass
class ActionInstance:
    id: int
    name: str
    parent: typing.Optional['ActionInstance']
    children: typing.List['ActionInstance']
    is_alive: bool


class LogGenerator:
    def __init__(self):
        self._root: typing.Final[ActionInstance] = ActionInstance(0, "root", None, [], True)
        self._actions: typing.List[ActionInstance] = [self._root]
        self._action_id: int = 1
        self._no_live_actions = 0

        self.first_message = []
        for i in range(random.randint(10, 200)):
            self.first_message.append(self.get_new_event())

    def get_child_of(self, id: int) -> typing.List[ActionInstance]:
        return [x for x in self._actions if x.parent.id == id and x.is_alive and x.id != 0]

    def sample_action(self) -> ActionInstance:
        '''
        Samples live action
        :return:
        '''
        return random.sample([x for x in self._actions if x.is_alive], 1)[0]

    def sample_non_root_action(self) -> ActionInstance:
        return random.sample([x for x in self._actions if x.id != 0 and x.is_alive], 1)[0]

    def sample_parent(self) -> typing.Optional[ActionInstance]:
        val = None
        try:
            val = random.sample([x for x in self._actions if
                                 x.id != 0 and
                                 x.is_alive and
                                 any([y.is_alive for y in x.children])], 1)[0]
        except Exception as e:
            pass
        return val

    def sample_child(self, parent_id: int) -> typing.Optional[ActionInstance]:
        val = None
        try:
            return \
                random.sample([x for x in self._actions if x.id != 0 and x.parent.id == parent_id and x.is_alive], 1)[0]
        except Exception as e:
            pass
        return val

    def sample_non_parent(self) -> ActionInstance:
        return random.sample([x for x in self._actions if
                              x.id != 0 and
                              x.is_alive and
                              not any([y.is_alive for y in x.children])], 1)[0]

    def sample_dead_action(self) -> typing.Optional[ActionInstance]:
        val = None
        try:
            return random.sample([x for x in self._actions if x.id != 0 and not x.is_alive], 1)[0]
        except:
            pass
        return val

    def event_action_created(self) -> dict:
        # choose parent
        new_action_id = self._action_id
        self._action_id = self._action_id + 1
        v = random.randint(0, 99)
        act = None
        if v < 30:
            # create a new instance of a dead action
            action = self.sample_dead_action()
            if action:
                action.is_alive = True
                action.id = new_action_id
                act = action
        if not act:
            parent = self.sample_action()
            act = ActionInstance(new_action_id, f"action{new_action_id}", parent, [], True)
            parent.children.append(act)
            self._actions.append(act)
        self._no_live_actions = self._no_live_actions + 1
        return {"time": time.time(), "from": act.id, "type": "_ac_c",
                "content": {"id": act.id, "parent_id": act.parent.id, "name": act.name,
                            "type": 0}}

    def event_action_deleted(self) -> typing.Optional[dict]:
        # choose parent
        action = self.sample_non_parent()
        if action:
            action.is_alive = False
            self._no_live_actions = self._no_live_actions - 1
            return {"time": time.time(), "from": action.id, "type": "_ac_t", "content": ""}
        else:
            return None

    def event_action_log(self) -> typing.Optional[dict]:
        action = self.sample_non_root_action()
        if not action:
            return None
        return {"time": time.time(), "from": action.id, "type": "_t", "content": lorem.sentence()}

    def event_action_feedback(self) -> typing.Optional[dict]:
        action = self.sample_parent()
        if not action:
            return None
        child = self.sample_child(action.id)
        if not child:
            return None
        return {"time": time.time(), "from": action.id, "type": "_fb_r",
                "content": {"from": child.id, "feedback": lorem.sentence()}}

    def get_new_event(self) -> dict:
        if self._no_live_actions == 0:
            return self.event_action_created()

        v = random.randint(0, 99)
        if v < 20 - min(self._no_live_actions, 20):
            return self.event_action_created()
        elif v < 20:
            dla = self.event_action_deleted()
            if dla:
                return dla
        elif v < 60:
            fb = self.event_action_feedback()
            if fb:
                return fb

        return self.event_action_log()


terminate = False


def send_msg(sock, msg):
    # Prefix each message with a 4-byte length (network byte order)
    msg = struct.pack('>I', len(msg)) + msg.encode()
    sock.sendall(msg)


def server_thread(conn, addr, lg):
    global terminate
    print(f"Connected by {addr}")
    with conn:
        cur_time = time.time()
        for m in reversed(lg.first_message):
            m['time'] = cur_time
            cur_time -= 0.5

        send_msg(conn, json.dumps(lg.first_message))
        try:
            while not terminate:
                send_msg(conn, json.dumps(lg.get_new_event()))
                time.sleep(0.5)
        except Exception as e:
            pass


if __name__ == '__main__':
    all_thread = []
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    try:
        s.bind(("localhost", 55555))
        print(f"Bound to socket {s.getsockname()}")
        s.listen()
        while not terminate:
            lg = LogGenerator()
            conn, addr = s.accept()
            all_thread.append(threading.Thread(target=server_thread, args=(conn, addr, lg)))
            all_thread[-1].start()
    finally:
        s.close()
        terminate = True
        # for a in all_thread:
        #    a.join()
# See PyCharm help at https://www.jetbrains.com/help/pycharm/
