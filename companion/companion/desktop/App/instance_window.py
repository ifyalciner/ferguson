#  MIT License
#
#  Copyright (c) 2023 Ibrahim Faruk YALCINER
#
#  Permission is hereby granted, free of charge, to any person obtaining a copy
#  of this software and associated documentation files (the "Software"), to deal
#  in the Software without restriction, including without limitation the rights
#  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#  copies of the Software, and to permit persons to whom the Software is
#  furnished to do so, subject to the following conditions:
#
#  The above copyright notice and this permission notice shall be included in all
#  copies or substantial portions of the Software.
#
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE

import typing

import PySide2.QtCore as Qc
import PySide2.QtGui as Qg
import PySide2.QtWidgets as Qw

from companion.core.tree import ActionInstance
from companion.core.utils import get_formatted_local_time_str
from . import graphics_data


class InstanceWindow(Qw.QMainWindow):
    sig_closing = Qc.Signal()

    def __init__(self, gd: graphics_data.GraphicsData, instance: ActionInstance):
        super().__init__()
        self.instance: typing.Final[ActionInstance] = instance
        self.gd = gd

        self.setupUi()
        self.show()

    def setupUi(self):
        self.setObjectName("Form")
        self.setMinimumSize(Qc.QSize(551, 610))
        self.verticalLayoutWidget = Qw.QWidget(self)
        self.verticalLayoutWidget.setGeometry(Qc.QRect(9, 9, 531, 591))
        self.verticalLayoutWidget.setObjectName("verticalLayoutWidget")
        self.verticalLayoutWidget.setSizePolicy(Qw.QSizePolicy.Expanding, Qw.QSizePolicy.Expanding)
        self.setCentralWidget(self.verticalLayoutWidget)

        self.verticalLayout = Qw.QVBoxLayout(self.verticalLayoutWidget)
        self.verticalLayout.setObjectName("verticalLayout")
        self.verticalLayout.setSizeConstraint(Qw.QLayout.SetDefaultConstraint)
        self.verticalLayout.setStretch(0, 1)
        self.verticalLayout.setStretch(1, 1)

        self.formLayout = Qw.QFormLayout()
        self.formLayout.setObjectName("formLayout")
        self.formLayout.setFieldGrowthPolicy(Qw.QFormLayout.ExpandingFieldsGrow)
        self.formLayout.setRowWrapPolicy(Qw.QFormLayout.DontWrapRows)
        # set formLayout to fill the whole verticalLayout horizontally
        self.formLayout.setSizeConstraint(Qw.QLayout.SetDefaultConstraint)
        self.formLayout.setFieldGrowthPolicy(Qw.QFormLayout.ExpandingFieldsGrow)
        self.verticalLayout.addLayout(self.formLayout)

        self.idLabel = Qw.QLabel(self.verticalLayoutWidget)
        self.idLabel.setObjectName("idLabel")
        self.formLayout.setWidget(0, Qw.QFormLayout.LabelRole, self.idLabel)
        self.idValueLabel = Qw.QLabel(self.verticalLayoutWidget)
        self.idValueLabel.setObjectName("idLineEdit")
        self.idValueLabel.setText(str(self.instance.id))
        self.idValueLabel.setTextInteractionFlags(Qc.Qt.TextSelectableByMouse)
        self.formLayout.setWidget(0, Qw.QFormLayout.FieldRole, self.idValueLabel)

        self.nameLabel = Qw.QLabel(self.verticalLayoutWidget)
        self.nameLabel.setObjectName("nameLabel")
        self.formLayout.setWidget(1, Qw.QFormLayout.LabelRole, self.nameLabel)
        self.nameValueLabel = Qw.QLabel(self.verticalLayoutWidget)
        self.nameValueLabel.setObjectName("nameLineEdit")
        self.nameValueLabel.setTextFormat(Qc.Qt.RichText)
        self.nameValueLabel.setText("<u>" + self.instance.name + "</u>")
        self.nameValueLabel.mouseDoubleClickEvent = \
            lambda x: self.openActionWindow(self.instance.name)
        self.formLayout.setWidget(1, Qw.QFormLayout.FieldRole, self.nameValueLabel)

        self.parentIDLabel = Qw.QLabel(self.verticalLayoutWidget)
        self.parentIDLabel.setObjectName("parentIDLabel")
        self.formLayout.setWidget(2, Qw.QFormLayout.LabelRole, self.parentIDLabel)
        self.parentIDValueLabel = Qw.QLabel(self.verticalLayoutWidget)
        self.parentIDValueLabel.setObjectName("parentIDLineEdit")
        self.parentIDValueLabel.setTextFormat(Qc.Qt.RichText)
        self.parentIDValueLabel.setText("<u>" + str(self.instance.parent_id) + "</u>")
        self.parentIDValueLabel.mouseDoubleClickEvent = \
            lambda x: self.openInstanceWindow(self.instance.parent_id)

        self.formLayout.setWidget(2, Qw.QFormLayout.FieldRole, self.parentIDValueLabel)

        self.createdAtLabel = Qw.QLabel(self.verticalLayoutWidget)
        self.createdAtLabel.setObjectName("createdAtLabel")
        self.formLayout.setWidget(4, Qw.QFormLayout.LabelRole, self.createdAtLabel)
        self.createdAtValueLabel = Qw.QLabel(self.verticalLayoutWidget)
        self.createdAtValueLabel.setObjectName("createdAtLineEdit")
        self.createdAtValueLabel.setText(get_formatted_local_time_str(self.instance.created_at))
        self.createdAtValueLabel.setTextInteractionFlags(Qc.Qt.TextSelectableByMouse)
        self.formLayout.setWidget(4, Qw.QFormLayout.FieldRole, self.createdAtValueLabel)

        self.destroyedAtLabel = Qw.QLabel(self.verticalLayoutWidget)
        self.destroyedAtLabel.setObjectName("destroyedAtLabel")
        self.formLayout.setWidget(5, Qw.QFormLayout.LabelRole, self.destroyedAtLabel)
        self.destroyedAtValueLabel = Qw.QLabel(self.verticalLayoutWidget)
        self.destroyedAtValueLabel.setObjectName("destroyedAtLineEdit")
        destroyed_at = self.instance.destroyed_at
        if destroyed_at == float('inf'):
            destroyed_at_str = ""
        else:
            destroyed_at_str = get_formatted_local_time_str(destroyed_at)
        self.destroyedAtValueLabel.setText(destroyed_at_str)
        self.destroyedAtValueLabel.setTextInteractionFlags(Qc.Qt.TextSelectableByMouse)
        self.destroyedAtValueLabel.setOpenExternalLinks(True)
        self.formLayout.setWidget(5, Qw.QFormLayout.FieldRole, self.destroyedAtValueLabel)

        self.returnedLabel = Qw.QLabel(self.verticalLayoutWidget)
        self.returnedLabel.setObjectName("returnedLabel")
        self.formLayout.setWidget(6, Qw.QFormLayout.LabelRole, self.returnedLabel)
        self.returnedLineEdit = Qw.QLineEdit(self.verticalLayoutWidget)
        self.returnedLineEdit.setObjectName("returnedLineEdit")
        self.returnedLineEdit.setText(str(self.instance.returned))
        self.returnedLineEdit.setReadOnly(True)
        self.formLayout.setWidget(6, Qw.QFormLayout.FieldRole, self.returnedLineEdit)

        self.logs_label = Qw.QLabel(self.verticalLayoutWidget)
        self.logs_label.setObjectName("logs_label")
        self.verticalLayout.addWidget(self.logs_label)
        # create new member called log_table with two columns as QTableWidget with 0 rows
        self.log_table = Qw.QTableWidget(0, 2, self.verticalLayoutWidget)
        self.log_table.setObjectName("log_table")
        self.log_table.setHorizontalHeaderLabels(["Time", "Log"])
        self.log_table.setColumnWidth(0, 20)
        self.log_table.setColumnWidth(1, 200)
        self.log_table.horizontalHeader().setStretchLastSection(True)
        self.log_table.horizontalHeader().setSectionResizeMode(Qw.QHeaderView.Stretch)
        self.log_table.setEditTriggers(Qw.QAbstractItemView.NoEditTriggers)
        self.log_table.setSelectionBehavior(Qw.QAbstractItemView.SelectRows)
        self.log_table.setSelectionMode(Qw.QAbstractItemView.SingleSelection)
        self.log_table.setShowGrid(False)
        self.log_table.verticalHeader().setVisible(False)
        self.log_table.setAlternatingRowColors(True)
        self.log_table.setSortingEnabled(False)
        self.log_table.setWordWrap(False)
        self.log_table.setCornerButtonEnabled(False)
        self.log_table.horizontalHeader().setSectionResizeMode(Qw.QHeaderView.ResizeToContents)
        self.verticalLayout.addWidget(self.log_table)

        self.retranslateUi()
        self.update()
        Qc.QMetaObject.connectSlotsByName(self)

    def retranslateUi(self):
        _translate = Qc.QCoreApplication.translate
        self.setWindowTitle(_translate(str(self.instance.id), f"Action Instance: {str(self.instance.id)}"))
        self.idLabel.setText(_translate(str(self.instance.id), "ID"))
        self.nameLabel.setText(_translate(str(self.instance.id), "Name"))
        self.parentIDLabel.setText(_translate(str(self.instance.id), "Parent ID"))
        self.createdAtLabel.setText(_translate(str(self.instance.id), "Created at"))
        self.destroyedAtLabel.setText(_translate(str(self.instance.id), "Destroyed at"))
        self.returnedLabel.setText(_translate(str(self.instance.id), "Returned"))
        self.logs_label.setText(_translate(str(self.instance.id), "Logs"))

    def update(self):
        """
        Here we update following fields:
            * destroyed at
            * no children
            * no instances
            * children list
            * instance list
        """
        # get number of items from self.log_table and compare it with number of logs in self.instance.all_logs
        # if they are not equal, then we need to update logs by appending
        if self.log_table.rowCount() != len(self.instance.all_logs):
            for log in self.instance.all_logs[self.log_table.rowCount():]:
                self.log_table.insertRow(self.log_table.rowCount())
                self.log_table.setItem(self.log_table.rowCount() - 1, 0,
                                       Qw.QTableWidgetItem(get_formatted_local_time_str(log[0])))
                self.log_table.setItem(self.log_table.rowCount() - 1, 1, Qw.QTableWidgetItem(str(log[1])))

        destroyed_at = self.instance.destroyed_at
        if destroyed_at != float('inf'):
            if self.destroyedAtValueLabel.text() == "":
                # Update only when we learn that we are destroyed
                destroyed_at_str = get_formatted_local_time_str(destroyed_at)
                self.destroyedAtValueLabel.setText(destroyed_at_str)

    def openInstanceWindow(self, instance_id: int, instance_name: str = None):
        if instance_name is None:
            # find instance_name by instance_id
            for name, instance in self.gd.action_graphic_objects.items():
                if instance_id in instance.instance_go:
                    instance_name = name
                    break
        self.gd.action_graphic_objects[instance_name].instance_go[instance_id].open_window()

    def openActionWindow(self, action_name: str):
        self.gd.action_graphic_objects[action_name].open_window()

    def closeEvent(self, event: Qg.QCloseEvent) -> None:
        self.sig_closing.emit()
        super().closeEvent(event)
