#  MIT License
#
#  Copyright (c) 2023 Ibrahim Faruk YALCINER
#
#  Permission is hereby granted, free of charge, to any person obtaining a copy
#  of this software and associated documentation files (the "Software"), to deal
#  in the Software without restriction, including without limitation the rights
#  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#  copies of the Software, and to permit persons to whom the Software is
#  furnished to do so, subject to the following conditions:
#
#  The above copyright notice and this permission notice shall be included in all
#  copies or substantial portions of the Software.
#
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE

import typing

import PySide2.QtCore as Qc
import PySide2.QtGui as Qg
import PySide2.QtWidgets as Qw


class FeedbackGOSigEmitter(Qc.QObject):
    sig_pos_updated = Qc.Signal()

    def __init__(self, parent):
        super().__init__()


class FeedbackGO(Qw.QGraphicsEllipseItem):
    radius = 13.0

    # Rectangle is relative to current objects pos
    # https://stackoverflow.com/questions/9002094/qgraphicsitem-returns-wrong-position-in-scene-qt4-7-3
    def __init__(self, orig_pos: Qc.QPointF, orig_time_resolution: float, content: str, parent):
        super(FeedbackGO, self).__init__(0, 0, self.radius, self.radius,
                                         parent=parent)
        self.setZValue(7)

        self.sig_emitter = FeedbackGOSigEmitter(self)

        self.orig_time_resolution: typing.Final[float] = orig_time_resolution
        self.orig_pos: typing.Final[Qc.QPointF] = orig_pos
        self._time_resolution: float = orig_time_resolution
        self.setPos(self.orig_pos)

        self.setToolTip(content)
        self.setBrush(Qc.Qt.blue)
        self.setFlag(Qw.QGraphicsItem.GraphicsItemFlag.ItemSendsScenePositionChanges)

    def update_time_resolution(self, time_resolution: float):
        self._time_resolution = time_resolution
        self.setPos((self.orig_pos.x() / self.orig_time_resolution) * self._time_resolution, self.pos().y())

    def itemChange(self, change: Qw.QGraphicsItem.GraphicsItemChange, value: typing.Any) -> typing.Any:
        ret = super().itemChange(change, value)
        if change == Qw.QGraphicsItem.GraphicsItemChange.ItemScenePositionHasChanged:
            self.sig_emitter.sig_pos_updated.emit()
        return ret


class FeedbackCounterGO(Qw.QGraphicsEllipseItem):
    radius = 0.2

    # Rectangle is relative to current objects pos
    # https://stackoverflow.com/questions/9002094/qgraphicsitem-returns-wrong-position-in-scene-qt4-7-3
    def __init__(self, orig_pos: Qc.QPointF, orig_time_resolution: float, fb_go: FeedbackGO, parent):
        super(FeedbackCounterGO, self).__init__(0, 0, 0, 0,
                                                parent=parent)
        self.setZValue(7)

        self.fb_go = fb_go
        # self.fb_go.sig_updated.connect(self.update_arrow)
        self.orig_time_resolution: typing.Final[float] = orig_time_resolution
        self.orig_pos: typing.Final[Qc.QPointF] = orig_pos
        self._time_resolution: float = orig_time_resolution
        self.setPos(self.orig_pos)
        # self.setOpacity(0.0)
        self.line: Qw.QGraphicsLineItem = None
        self.draw_line()
        # self.visibleChanged.connect(self.visibility_changed)
        self.setFlag(Qw.QGraphicsItem.GraphicsItemFlag.ItemSendsScenePositionChanges)
        self.fb_go.sig_emitter.sig_pos_updated.connect(self.update_arrow)
        # self.fb_go._pos_changed = self.update_arrow

    def update_time_resolution(self, time_resolution: float):
        self._time_resolution = time_resolution
        self.setPos((self.orig_pos.x() / self.orig_time_resolution) * self._time_resolution, self.pos().y())
        self.fb_go.update_time_resolution(time_resolution)
        self.update_arrow()

    def itemChange(self, change: Qw.QGraphicsItem.GraphicsItemChange, value: typing.Any) -> typing.Any:
        # self.sig_updated.emit()
        ret = super().itemChange(change, value)
        if change == Qw.QGraphicsItem.GraphicsItemChange.ItemScenePositionHasChanged:
            self.update_arrow()
        return ret

    def update_arrow(self):
        self.draw_line()

    def draw_line(self):
        to = self.fb_go.sceneBoundingRect().center()
        diff = to - self.scenePos()
        if self.line:
            self.line.setVisible(False)
        self.line = Qw.QGraphicsLineItem(diff.x(), 0, diff.x(), diff.y(), parent=self)
        pfine = Qg.QPen(Qg.QColor(100, 100, 200, 255), 2)
        self.line.setPen(pfine)
        # self.line.setOpacity(1.0)
        self.line.setZValue(-1)
        self.line.setVisible(True)

    # def itemChange(self, change:Qw.QGraphicsItem.GraphicsItemChange, value:typing.Any) -> typing.Any:
    #    pass
