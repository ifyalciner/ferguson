// MIT License
//
// Copyright (c) 2022 Ibrahim Faruk YALCINER
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

#include <gtest/gtest.h>

#include <iostream>
#include <memory>
#include <random>

#include <ferguson/synchronizers/interruptable.h>

#define GTEST_OUT std::cout << "[          ] [ INFO ] "

using namespace Ferguson;
using namespace Ferguson::Synchronizer;

/**
 * @TODO Tests for throwException and  threadException functions are missing
 */

class InterruptableTest: public ::testing::Test {
public:
  void
  SetUp() override {
    max_allowed_delay                   = TimeUnit(getRandomDelay());
    std::tie(sync_ptr, sync_thread_ptr) = interruptableFactory(max_allowed_delay);
    GTEST_OUT << "Delay " << max_allowed_delay.count() << "ms" << std::endl;
  }

  void
  TearDown() override {
    max_allowed_delay = TimeUnit(0);
    sync_ptr          = nullptr;
    sync_thread_ptr   = nullptr;
  }

  static uint64_t
  getRandomDelay() {
    // Generate random delay
    std::random_device rd;
    std::mt19937 mt(rd());
    std::uniform_int_distribution<uint64_t> dist(10, 100);
    return dist(mt);
  }

  InterruptableHostSPtr sync_ptr;
  InterruptableThreadSPtr sync_thread_ptr;
  TimeUnit max_allowed_delay{};
};

TEST_F(InterruptableTest, CheckInitializedValues) {
  EXPECT_EQ(sync_ptr->isInitialized(), false);
  EXPECT_EQ(sync_ptr->isTerminated(), false);
}

TEST_F(InterruptableTest, TermBeforeInit) {
  uint64_t counter    = 0;
  auto incrementor    = [&]() { counter++; };
  auto thread_routine = [sync_ptr = sync_thread_ptr, incrementor] { interruptableRun(sync_ptr, incrementor); };
  boost::thread th(thread_routine);
  auto start = Clock::now();
  sync_ptr->requestTerminate();
  if(th.joinable()) {
    th.join();
  }
  auto finish = Clock::now();
  EXPECT_TRUE(boost::chrono::duration_cast<TimeUnit>(finish - start) <= max_allowed_delay);
  EXPECT_EQ(counter, 0);
  EXPECT_EQ(sync_ptr->isInitialized(), false);
  EXPECT_EQ(sync_ptr->isTerminated(), true);
}

TEST_F(InterruptableTest, Init) {
  uint64_t counter    = 0;
  auto incrementor    = [&]() { counter++; };
  auto thread_routine = [sync_ptr = sync_thread_ptr, incrementor] { interruptableRun(sync_ptr, incrementor); };
  boost::thread th(thread_routine);
  auto start = Clock::now();
  EXPECT_EQ(counter, 0);
  sync_ptr->requestInitialize();
  if(th.joinable()) {
    th.join();
  }
  auto finish = Clock::now();
  EXPECT_TRUE(boost::chrono::duration_cast<TimeUnit>(finish - start) <= max_allowed_delay);
  EXPECT_EQ(counter, 1);
  EXPECT_EQ(sync_ptr->isInitialized(), true);
  EXPECT_EQ(sync_ptr->isTerminated(), true);
}

TEST_F(InterruptableTest, InitThenTerm) {
  uint64_t counter = 0;
  auto incrementor = [&]() {
    boost::this_thread::sleep_for(TimeUnit(1));
    try {
      while(!sync_thread_ptr->isTerminateRequested()) {
        counter++;
      }
    }
    catch(...) {
    }
  };
  auto thread_routine = [sync_ptr = sync_thread_ptr, incrementor] { interruptableRun(sync_ptr, incrementor); };
  boost::thread th(thread_routine);
  sync_ptr->requestInitialize();
  sync_ptr->requestTerminate();
  if(th.joinable()) {
    th.join();
  }
  EXPECT_EQ(counter, 0);
  EXPECT_EQ(sync_ptr->isInitialized(), true);
  EXPECT_EQ(sync_ptr->isTerminated(), true);
}

TEST_F(InterruptableTest, WaitTerminated) {
  auto waitor = [&]() {
    Ferguson::sleepFor(TimeUnit(max_allowed_delay - TimeUnit(2)));
    sync_thread_ptr->setTerminated();
  };
  auto thread_routine = [sync_ptr = sync_thread_ptr, waitor] { interruptableRun(sync_ptr, waitor); };
  boost::thread th(thread_routine);
  sync_ptr->requestInitialize();
  sync_ptr->requestTerminate();
  sync_ptr->waitTerminated();
  EXPECT_EQ(sync_ptr->isTerminated(), true);
  // Obligatory join
  if(th.joinable()) {
    th.join();
  }
}

TEST_F(InterruptableTest, LateTerminateCheckException) {
#ifndef DEBUGGING
  // This test is disabled until I decide how to handle run time warnings/errors
  //uint64_t counter = 0;
  //auto incrementor = [&]() {
  //  Ferguson::sleepFor(max_allowed_delay + TimeUnit(100));
  //  // Too late in Termination check
  //  EXPECT_THROW({ sync_thread_ptr->isTerminateRequested(); }, Ferguson::Synchronizer::response_timeout_error);
  //};
  //auto thread_routine = std::bind(InterruptableRun, sync_thread_ptr, incrementor);
  //boost::thread th(thread_routine);
  //sync_ptr->requestInitialize();
  //if(th.joinable()) {
  //  th.join();
  //}
  //EXPECT_EQ(counter, 0);
#endif
}
