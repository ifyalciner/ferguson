// MIT License
//
// Copyright (c) 2022 Ibrahim Faruk YALCINER
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

#include <gtest/gtest.h>

#include <thread>

#include <ferguson/synchronizers/anytime.h>

#define GTEST_OUT std::cout << "[          ] [ INFO ] "

using namespace Ferguson;
using namespace Ferguson::Synchronizer;

struct MResult {
  explicit MResult(int i): i(i) {}

  int i;
  DEFINE_SMARTPTR(MResult)
};

struct MProduct {
  explicit MProduct(int i): i(i) {}

  int i;

  DEFINE_SMARTPTR(MProduct)
};

TEST(FunctionTest, PublishProductQueue) {
  AnytimeHostSPtr sync_ptr;
  AnytimeThreadSPtr sync_thread_ptr;
  std::tie(sync_ptr, sync_thread_ptr) = anytimeFactory(TimeUnit(10));

  int counter_limit  = 100;
  auto function_task = [&]() {
    for(int i = 0; i < counter_limit; i++) {
      sync_thread_ptr->publishProduct(MProduct(i));
    }
    return;
  };
  auto thread_routine = [sync_thread_ptr, function_task] { interruptableRun(sync_thread_ptr, function_task); };
  std::thread th(thread_routine);

  sync_ptr->requestInitialize();
  if(th.joinable()) {
    th.join();
  }
  // At this point thread is dead. Products are queued.
  for(int i = 0; i < counter_limit; i++) {
    auto x = sync_ptr->getProduct(TimePoint::max());
    EXPECT_TRUE(x.has_value());
    if(x.has_value()) {
      EXPECT_EQ(std::any_cast<MProduct>(x).i, i);
    }
  }
}

TEST(FunctionTest, PublishProductQueueTimeout) {
  AnytimeHostSPtr sync_ptr;
  AnytimeThreadSPtr sync_thread_ptr;
  std::tie(sync_ptr, sync_thread_ptr) = anytimeFactory(TimeUnit(10));

  int counter_limit  = 10;
  auto function_task = [&]() {
    for(int i = 0; i < counter_limit; i++) {
      Ferguson::sleepFor(TimeUnit(3));
      sync_thread_ptr->publishProduct(MProduct(i));
    }
    return;
  };
  auto thread_routine = [sync_thread_ptr, function_task] { interruptableRun(sync_thread_ptr, function_task); };
  std::thread th(thread_routine);

  sync_ptr->requestInitialize();
  /* Generation of each message takes 3ms, first two attempts of getting feedback should timeout on 1ms only third
   * to be successfully returning */
  for(int i = 0; i < counter_limit; i++) {
    // First ms
    {
      auto x = sync_ptr->getProduct(toDeadline(TimeUnit(1)));
      EXPECT_FALSE(x.has_value());
    }

    {
      //Second ms
      auto x = sync_ptr->getProduct(toDeadline(TimeUnit(1)));
      EXPECT_FALSE(x.has_value());
    }

    {
      //Third ms, should be ready now
      auto x = sync_ptr->getProduct(toDeadline(TimeUnit(2)));
      EXPECT_TRUE(x.has_value());
      if(x.has_value()) {// check is to prevent segfaults in test
        EXPECT_EQ(std::any_cast<MProduct>(x).i, i);
      }
    }
  }
  if(th.joinable()) {
    th.join();
  }
}

TEST(FunctionTest, PublishResultDone) {
  AnytimeHostSPtr sync_ptr;
  AnytimeThreadSPtr sync_thread_ptr;
  std::tie(sync_ptr, sync_thread_ptr) = anytimeFactory(TimeUnit(10));

  int counter_limit  = 100;
  auto function_task = [&]() {
    for(int i = 0; i < counter_limit; i++) {
      sync_thread_ptr->publishProduct(MProduct(i));
    }
    return;
  };
  auto thread_routine = [sync_thread_ptr, function_task] { interruptableRun(sync_thread_ptr, function_task); };
  std::thread th(thread_routine);
  sync_ptr->requestInitialize();
  while(!sync_ptr->isTerminated()) {}
  auto start = Clock::now();
  auto end   = Clock::now();
  EXPECT_TRUE(end - start <= TimeUnit(1));
  if(th.joinable()) {
    th.join();
  }
}

