// MIT License
//
// Copyright (c) 2022 Ibrahim Faruk YALCINER
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include <gtest/gtest.h>

#include <chrono>
#include <iostream>
#include <memory>
#include <random>
#include <thread>

#include <ferguson/state/delayed_bool.h>

#define GTEST_OUT std::cout << "[          ] [ INFO ] "

using namespace Ferguson::State;

TEST(DelayedBoolTest, ZeroTest) {
  // In this test DelayedBool should pretend exactly like bool
  auto db = DelayedBool::factory(false);
  EXPECT_FALSE(*db);
  *db = true;
  EXPECT_TRUE(*db);
  *db = false;
  EXPECT_FALSE(*db);
}

TEST(DelayedBoolTest, TrueDelayTest) {
  auto delay = Ferguson::TimeUnit(500);
  auto db    = DelayedBool::factory(false, delay);
  EXPECT_FALSE(*db);
  *db     = true;
  auto dl = Ferguson::toDeadline(delay, Ferguson::Clock::now());
  EXPECT_FALSE(*db);
  Ferguson::sleepUntil(dl);
  EXPECT_TRUE(*db);
}

TEST(DelayedBoolTest, TrueDelayTest2) {
  auto delay = Ferguson::TimeUnit(500);
  auto db    = DelayedBool::factory(false, delay);
  EXPECT_FALSE(*db);
  *db     = true;
  auto dl = Ferguson::toDeadline(delay, Ferguson::Clock::now());
  EXPECT_FALSE(*db);
  *db = false;
  Ferguson::sleepUntil(dl);
  EXPECT_FALSE(*db);
}

TEST(DelayedBoolTest, FalseDelayTest) {
  auto delay = Ferguson::TimeUnit(500);
  auto db    = DelayedBool::factory(true, Ferguson::TimeUnit(0), delay);
  auto dl    = Ferguson::toDeadline(delay, Ferguson::Clock::now());
  EXPECT_TRUE(*db);
  *db = false;
  EXPECT_TRUE(*db);
  Ferguson::sleepUntil(dl);
  EXPECT_FALSE(*db);
}

TEST(DelayedBoolTest, setImmediately) {
  auto delay = Ferguson::TimeUnit(1000);
  auto db    = DelayedBool::factory(true, delay, delay);
  // This is a delayed set
  EXPECT_TRUE(*db);
  db->setImmediately(false);
  EXPECT_FALSE(*db);
}

TEST(DelayedBoolTest, setImmediately2) {
  auto delay = Ferguson::TimeUnit(1000);
  auto db    = DelayedBool::factory(true, delay, delay);
  // This is a delayed set, and it will be removed by setImmediately
  *db     = false;
  auto dl = Ferguson::toDeadline(delay, Ferguson::Clock::now());
  db->setImmediately(true);
  EXPECT_TRUE(*db);
  Ferguson::sleepUntil(dl);
  // After the delay time this still will remain true
  EXPECT_TRUE(*db);
}

TEST(DelayedBoolTest, JitterTest) {
  auto delay  = Ferguson::TimeUnit(200);
  auto jitter = Ferguson::TimeUnit(50);
  auto db     = DelayedBool::factory(false, delay, delay, jitter);
  *db         = true;
  auto dl     = Ferguson::toDeadline(delay, Ferguson::Clock::now());
  Ferguson::sleepFor(delay / 3);
  *db = false;// This would normally reset the timer and at the time `dl` we wouldn't have *db == true
  Ferguson::sleepFor(jitter / 2);
  *db = true;
  EXPECT_FALSE(*db);// Should still be false at this point
  Ferguson::sleepUntil(dl);
  EXPECT_TRUE(*db);
}

TEST(DelayedBoolTest, JitterTest2) {
  auto delay  = Ferguson::TimeUnit(200);
  auto jitter = Ferguson::TimeUnit(50);
  auto db     = DelayedBool::factory(false, delay, delay, jitter);
  *db         = true;
  auto dl     = Ferguson::toDeadline(delay + Ferguson::TimeUnit(10), Ferguson::Clock::now());
  Ferguson::sleepUntil(dl);
  EXPECT_TRUE(*db);
}

TEST(DelayedBoolTest, JitterTest3) {
  auto delay  = Ferguson::TimeUnit(400);
  auto jitter = Ferguson::TimeUnit(50);
  auto db     = DelayedBool::factory(false, delay, delay, jitter);
  EXPECT_FALSE(*db);
  //
  *db     = true;
  auto dl = Ferguson::toDeadline(delay, Ferguson::Clock::now());
  Ferguson::sleepFor(jitter);// Wait for amount of activation_delay for `true` to be accepted
  for(int i = 0; i < 3; i++) {
    *db = false;
    EXPECT_FALSE(*db);
    Ferguson::sleepFor(jitter / 2);
    *db = true;
    EXPECT_FALSE(*db);
    Ferguson::sleepFor(jitter / 2);
  }
  // We have ended in false, so when false time kicks in after false window is over, db will set to false again
  *db      = false;
  auto dl2 = Ferguson::toDeadline(delay, Ferguson::Clock::now());
  // check if we have reached the deadline already
  // This is not the most accurate way to check but it is good enough for this test
  auto cur_time = Ferguson::Clock::now();
  if(dl <= cur_time && cur_time < dl2) {
    Ferguson::sleepUntil(dl);
    // Although we have ended with false,
    EXPECT_TRUE(*db);
  }
  if(dl2 < Ferguson::Clock::now()) {
    Ferguson::sleepUntil(dl2);
    EXPECT_FALSE(*db);
  }
}
