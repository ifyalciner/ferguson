// MIT License
//
// Copyright (c) 2022 Ibrahim Faruk YALCINER
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

#include <gtest/gtest.h>

#include <memory>
#include <sstream>
#include <thread>
#include <utility>

#include "../../src/threads/action_impl.h"
#include <ferguson/threads/action.h>

#define GTEST_OUT std::cout << "[          ] [ INFO ] "

using namespace Ferguson;

class MyAction {
public:
  explicit MyAction(int i): goal_i(i) {}
  virtual ~MyAction() { GTEST_OUT << "Action terminated" << std::endl; }

  void
  operator()(std::function<void(int)> const &product_callback) {
    for(int i = 0; i < goal_i; ++i) {
      Ferguson::this_action::sleepFor(TimeUnit(9));
      GTEST_OUT << "Counting: " << i << " Time: " << Ferguson::Clock::now().time_since_epoch().count() << std::endl;
      product_callback(i);
    }
  }

private:
  int goal_i;
};

TEST(ActionTest, ExecutionWait) {
  const int counter_limit = 5;
  auto my_action = Ferguson::Threads::make_action(Ferguson::Threads::make_shared_task_object<MyAction>(counter_limit),//
                                                  Threads::Action::root(),                                            //
                                                  "MyAction",                                                         //
                                                  TimeUnit(10));

  int i = 0;
  while(true) {
    auto fb = my_action.getProduct();
    if(!fb) {
      EXPECT_TRUE(my_action.finished());
      break;
    }
    EXPECT_TRUE(fb);
    EXPECT_EQ(fb.value(), i++);
  }
}

TEST(ActionTest, ExecutionCallbacks) {
  const int counter_limit = 5;
  int i                   = 0;
  int tested_fb           = 0;

  auto fb_cb = [&](int fb) {
    EXPECT_EQ(fb, i++);
    ++tested_fb;
  };

  auto my_action = Ferguson::Threads::make_action(Ferguson::Threads::make_shared_task_object<MyAction>(counter_limit),//
                                                  Threads::Action::root(),                                            //
                                                  "MyAction",                                                         //
                                                  TimeUnit(10),                                                       //
                                                  fb_cb);

  // Ideal way of waiting for my_action is through `waitResult` in synchronous operations like this test,
  // if preemption will not be requested (if preemption is requested `waitResult` will still return)
  my_action.waitUntilFinished();
  // alternatively we can use following command
  while(!my_action.finished()) {
    Ferguson::sleepFor(TimeUnit(10));
  }
  EXPECT_EQ(tested_fb, 5);

  /* DIFFERENCE BETWEEN done() And waitResult()
   * done() returns true whether we received a result or terminated
   * waitResult() returns non-nullptr when result is received or terminated just like done()
   *      but with a timeout argument it will return nontheless which doesn't indicate that we are done.
   */
}

TEST(ActionTest, ExecutionTerminate) {
  const int counter_limit    = 100;
  int i                      = 0;
  std::atomic<int> tested_fb = 0;

  auto fb_cb = [&](int const &fb) {
    EXPECT_EQ(fb, i++);
    ++tested_fb;
  };

  auto my_action = Ferguson::Threads::make_action(Ferguson::Threads::make_shared_task_object<MyAction>(counter_limit),//
                                                  Threads::Action::root(),                                            //
                                                  "MyAction",                                                         //
                                                  TimeUnit(10),                                                       //
                                                  fb_cb);

  // Ideal way of waiting for my_action is through `waitResult` in synchronous operations like this test,
  // if preemption will not be requested (if preemption is requested `waitResult` will still return)
  // It is guaranteed that we will return in 10ms also each counting takes 9 ms
  // So we may or may not execute the third count up
  while(tested_fb <= 2) {
    Ferguson::sleepFor(TimeUnit(1));
  }
  my_action.preempt();
  EXPECT_GE(tested_fb, 2);
  EXPECT_LE(tested_fb, 3);
}

TEST(ActionTest, ExecutionAutoTerminate) {
  const int counter_limit = 100;
  int i                   = 0;
  int tested_fb           = 0;

  {
    auto fb_cb = [&](int const &fb) {
      EXPECT_EQ(fb, i++);
      ++tested_fb;
    };

    auto my_action =
        Ferguson::Threads::make_action(Ferguson::Threads::make_shared_task_object<MyAction>(counter_limit),//
                                       Threads::Action::root(),                                            //
                                       "MyAction",                                                         //
                                       TimeUnit(10),                                                       //
                                       fb_cb);
    Ferguson::sleepFor(TimeUnit(30));
    //In 30ms sleep we can do 2 to 4 counting
  }//Auto terminate upon destructing object
  EXPECT_GE(tested_fb, 2);
  EXPECT_LE(tested_fb, 4);
}

TEST(ActionTest, ExecutionIterator) {
  const int counter_limit = 5;
  auto my_action = Ferguson::Threads::make_action(Ferguson::Threads::make_shared_task_object<MyAction>(counter_limit),//
                                                  Threads::Action::root(),                                            //
                                                  "MyAction",                                                         //
                                                  TimeUnit(10));
  //

  int i = 0;
  for(auto fb : my_action) {
    EXPECT_EQ(fb, i++);
  }
  my_action.waitUntilFinished();
}

TEST(ActionTest, ExecutionConstIterator) {
  const int counter_limit = 5;
  auto my_action = Ferguson::Threads::make_action(Ferguson::Threads::make_shared_task_object<MyAction>(counter_limit),//
                                                  Threads::Action::root(),                                            //
                                                  "MyAction",                                                         //
                                                  TimeUnit(10));

  int i = 0;
  for(auto const &fb : my_action) {
    EXPECT_EQ(fb, i++);
  }
  my_action.waitUntilFinished();
}

class MyActionThrowing {
public:
  MyActionThrowing() = default;
  ~MyActionThrowing() { GTEST_OUT << "Action terminated" << std::endl; }

  void
  operator()(std::function<void(int)> const &product_callback) {
    throw std::runtime_error("MyActionThrowing");
  }
};
TEST(ActionTest, ThrowingAction) {
  // immediate raise
  auto my_action = Ferguson::Threads::make_action(Ferguson::Threads::make_shared_task_object<MyActionThrowing>(),//
                                                  Threads::Action::root(),                                       //
                                                  "MyAction",                                                    //
                                                  TimeUnit(10));
  for(auto &fb : my_action) {
    GTEST_OUT << "Product: " << fb << std::endl;
  }
  try {
    my_action.throwException();
  }
  catch(std::runtime_error const &e) {
    EXPECT_STREQ(e.what(), "MyActionThrowing");
  }
}

class MyActionResourceAllocation {
private:
  int **_resource;

public:
  explicit MyActionResourceAllocation(int **resource): _resource(resource) { *this->_resource = new int(100); }
  ~MyActionResourceAllocation() {
    GTEST_OUT << "Action terminated" << std::endl;
    delete *_resource;
    *_resource = nullptr;
  }

  void
  operator()(std::function<void(int)> const &product_callback) {
    Ferguson::this_action::sleepFor(TimeUnit(9));
    throw std::runtime_error("MyActionThrowing");
  }
};

TEST(ActionTest, ResourceCleanup) {
  // immediate raise
  int **resource = new int *;
  auto my_action =
      Ferguson::Threads::make_action(Ferguson::Threads::make_shared_task_object<MyActionResourceAllocation>(resource),//
                                     Threads::Action::root(),                                                         //
                                     "MyAction",                                                                      //
                                     TimeUnit(10));
  my_action.waitUntilFinished();
  try {
    my_action.throwException();
    FAIL() << "Exception should be thrown";
  }
  catch(std::runtime_error const &e) {
    EXPECT_STREQ(e.what(), "MyActionThrowing");
  }
  catch(std::exception const &e) {
    // Print unexpected exceptions type
    FAIL() << "Unexpected exception type: " << e.what();
  }
  EXPECT_EQ(*resource, nullptr);
}

TEST(ActionTest, LambdaExecutionTerminate) {
  const int counter_limit = 5;
  int i                   = 0;
  int tested_fb           = 0;

  auto fb_cb = [&](int fb) {
    EXPECT_EQ(fb, i++);
    ++tested_fb;
  };

  auto my_action = Ferguson::Threads::make_action(
      Ferguson::Threads::make_shared_task_function([goal_i = counter_limit](
                                                       std::function<void(int)> const &product_callback) {
        for(int i = 0; i < goal_i; ++i) {
          Ferguson::this_action::sleepFor(TimeUnit(9));
          GTEST_OUT << "Counting: " << i << " Time: " << Ferguson::Clock::now().time_since_epoch().count() << std::endl;
          product_callback(i);
        }
      }),                     //
      Threads::Action::root(),//
      "MyActionLambda",       //
      TimeUnit(10),           //
      fb_cb);

  // Ideal way of waiting for my_action is through `waitResult` in synchronous operations like this test,
  // if preemption will not be requested (if preemption is requested `waitResult` will still return)
  // It is guaranteed that we will return in 10ms also each counting takes 9 ms
  // So we may or may not execute the third count up
  while(tested_fb <= 2) {
    Ferguson::sleepFor(TimeUnit(1));
  }
  my_action.preempt();
  EXPECT_GE(tested_fb, 2);
  EXPECT_LE(tested_fb, 3);
}

//TEST(ActionTest, LOG) {
//  uint64_t id;
//  MResultCSPtr res;
//  {
//    auto my_action = MyAction::factory(1, Threads::Action::root(), 10);
//    id             = my_action->id();
//    res            = Ferguson::as<MResult const>(my_action->waitResult()).value();
//  }
//  {
//    GTEST_OUT << "Full logMessage string: " << log_string << std::endl;
//    //deserialize log_string containing JSON
//    boost::property_tree::ptree log;
//    std::stringstream ss(log_string);
//    boost::property_tree::read_json(ss, log);
//    EXPECT_EQ(log.get<std::string>("action_name"), "MyActionTest");
//    EXPECT_EQ(log.get<std::string>("action_class"), "::MyAction");
//    EXPECT_EQ(log.get<std::string>("id"), std::to_string(id));
//    EXPECT_EQ(log.get<std::string>("pid"), std::to_string(Threads::Action::root().id));
//    EXPECT_EQ(log.get<std::string>("message"), "Initialized");
//  }
//  {
//
//    auto res_json = res->to_json();
//    EXPECT_EQ(res_json.get<int>("i"), 1 + 1);// +1 is because when we exceed loop will stop
//    EXPECT_EQ(res_json.get<std::string>("exception"), "null");
//  }
//}
//
