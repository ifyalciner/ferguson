// MIT License
//
// Copyright (c) 2022 Ibrahim Faruk YALCINER
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

#include <gtest/gtest.h>

#include <memory>
#include <thread>
#include <utility>

#include "../../src/threads/yielding_action_impl.h"

#define GTEST_OUT std::cout << "[          ] [ INFO ] "

namespace YieldingAction {
using namespace Ferguson;

struct MFeedback: public Threads::ActionFeedback {
  int i;

  explicit MFeedback(int i): i(i) {}

  [[nodiscard]] boost::property_tree::ptree
  to_json() const final {
    boost::property_tree::ptree pt = Threads::ActionFeedback::to_json();
    pt.put("i", i);
    return pt;
  }
};
DEFINE_SMARTPTR(MFeedback)

struct MResult: public Threads::ActionResult {
  int i;

  explicit MResult(int i): i(i) {}

  [[nodiscard]] boost::property_tree::ptree
  to_json() const final {
    boost::property_tree::ptree pt = Ferguson::Threads::ActionResult::to_json();
    pt.put("i", i);
    return pt;
  }
};
DEFINE_SMARTPTR(MResult);

class MyYF: public Threads::YieldingActionImpl {
public:
  static std::shared_ptr<Action>
  factory(int i,                                                       //
          int timeout_ms,                                              //
          std::optional<Threads::ActionFeedbackCB> fbcb = std::nullopt,//
          std::optional<Threads::ActionResultCB> rescb  = std::nullopt) {
    return std::shared_ptr<Action>(new MyYF(i, timeout_ms, std::move(fbcb), std::move(rescb)));
  }

  ~MyYF() override { stopAction(); }

protected:
  MyYF(int i,                                                              //
       int timeout_ms,                                                     //
       std::optional<Threads::ActionFeedbackCB> const &fbcb = std::nullopt,//
       std::optional<Threads::ActionResultCB> const &rescb  = std::nullopt)
      : Threads::YieldingActionImpl(Threads::Action::root(),//
                                    "YieldingActionTest",   //
                                    GET_TYPE(),             //
                                    TimeUnit(timeout_ms),   //
                                    fbcb,                   //
                                    rescb),                 //
        goal_i(i) {
    runAction();
  }

  Threads::ActionResultCSPtr
  run(Threads::ActionFeedbackCB const &publish_feedback) override {
    for(int i = 0; i < goal_i; ++i) {
      Ferguson::sleepFor(TimeUnit(9));
      publish_feedback(std::dynamic_pointer_cast<Threads::ActionFeedback const>(std::make_shared<MFeedback const>(i)));
    }
    return std::dynamic_pointer_cast<Threads::ActionResult>(std::make_shared<MResult>(goal_i + 1));
  }

private:
  int goal_i;
};

TEST(YieldingActionTest, ExecutionWait) {
  const int counter_limit = 5;
  auto my_action          = MyYF::factory(counter_limit, 10);

  int i = 0;
  while(true) {
    auto fb = as<MFeedback const>(my_action->waitFeedback(TimePoint::max()));
    if(!fb) {
      EXPECT_TRUE(my_action->finished());
      break;
    }
    EXPECT_TRUE(fb.value());
    EXPECT_EQ(fb.value()->i, i++);
  }
  auto res = as<MResult const>(my_action->waitResult());
  EXPECT_TRUE(res);
  EXPECT_NE(res.value(), MResultCSPtr(nullptr));
  EXPECT_EQ(res.value()->i, counter_limit + 1);
}

TEST(YieldingActionTest, ExecutionGet) {
  const int counter_limit = 1000;
  auto my_action          = MyYF::factory(counter_limit, 10);

  auto start = Clock ::now();
  auto fb    = as<MFeedback const>(my_action->getFeedback());
  auto end   = Clock ::now();
  EXPECT_TRUE(fb) << "No limitations in call (ie Timeout) so this shouldn't fail.";
  EXPECT_FALSE(fb.value()) << "It takes 9ms to generate feedback, there shouldn't be any in first call";
  EXPECT_TRUE(std::chrono::duration_cast<TimeUnit>(end - start).count() <= 1)
      << "Shouldn't exceed 1ms, although timeout is set to max (by default)";
}

TEST(YieldingActionTest, ExecutionCallbacks) {
  const int counter_limit = 5;
  int i                   = 0;
  int tested_fb           = 0;
  int tested_res          = 0;

  auto fb_cb = [&](Function::FeedbackCSPtr const &fb) {
    auto cfb = std::dynamic_pointer_cast<MFeedback const>(fb);
    EXPECT_NE(cfb, MFeedbackCSPtr(nullptr));
    EXPECT_EQ(cfb->i, i++);
    ++tested_fb;
  };

  auto res_cb = [&](Function::ResultCSPtr const &res) {
    auto cres = std::dynamic_pointer_cast<MResult const>(res);
    EXPECT_EQ(cres->i, counter_limit + 1);
    ++tested_res;
  };

  auto my_action = MyYF::factory(counter_limit, 10, fb_cb, res_cb);

  // Ideal way of waiting for my_action is through `waitResult` in synchronous operations like this test,
  // if preemption will not be requested (if preemption is requested `waitResult` will still return)
  my_action->waitResult();
  // alternatively we can use following command
  while(!my_action->finished()) {
    Ferguson::sleepFor(TimeUnit(10));
  }
  EXPECT_EQ(tested_fb, 5);
  EXPECT_EQ(tested_res, 1);

  /* DIFFERENCE BETWEEN done() And waitResult()
   * done() returns true whether we received a result or terminated
   * waitResult() returns non-nullptr when result is received or terminated just like done()
   *      but with a timeout argument it will return nontheless which doesn't indicate that we are done.
   */
}

TEST(YieldingActionTest, ExecutionTerminate) {
  const int counter_limit = 100;
  int i                   = 0;
  int tested_fb           = 0;
  int tested_res          = 0;
  std::shared_ptr<Ferguson::Threads::Action> my_action;

  auto fb_cb = [&](Function::FeedbackCSPtr const &fb) {
    auto cfb = std::dynamic_pointer_cast<MFeedback const>(fb);
    EXPECT_NE(cfb, MFeedbackCSPtr(nullptr));
    EXPECT_EQ(cfb->i, i++);
    ++tested_fb;
    if(i >= 2) {
      my_action->preempt();
    }
  };

  auto res_cb = [&](Function::ResultCSPtr const &res) {
    auto cres = std::dynamic_pointer_cast<MResult const>(res);
    EXPECT_NE(cres, MResultCSPtr(nullptr));
    EXPECT_EQ(cres->i, counter_limit + 1);
    ++tested_res;
  };
  my_action = MyYF::factory(counter_limit, 10, fb_cb, res_cb);

  // Ideal way of waiting for my_action is through `waitResult` in synchronous operations like this test,
  // if preemption will not be requested (if preemption is requested `waitResult` will still return)
  my_action->waitResult();
  // It is guaranteed that we will return in 10ms also each counting takes 9 ms
  // So we may or may not execute the third count up
  EXPECT_GE(tested_fb, 2);
  EXPECT_LE(tested_fb, 3);
  // We are preempting in feedback, result shouldn't be executed
  EXPECT_EQ(tested_res, 0);
}

TEST(YieldingActionTest, ExecutionAutoTerminate) {
  const int counter_limit = 100;
  int i                   = 0;
  int tested_fb           = 0;
  int tested_res          = 0;

  {
    auto fb_cb = [&](Function::FeedbackCSPtr const &fb) {
      auto cfb = std::dynamic_pointer_cast<MFeedback const>(fb);
      EXPECT_NE(cfb, MFeedbackCSPtr(nullptr));
      EXPECT_EQ(cfb->i, i++);
      ++tested_fb;
    };

    auto res_cb = [&](Function::ResultCSPtr const &res) {
      auto cres = std::dynamic_pointer_cast<MResult const>(res);
      EXPECT_NE(cres, MResultCSPtr(nullptr));
      EXPECT_EQ(cres->i, counter_limit + 1);
      ++tested_res;
    };

    auto my_action = MyYF::factory(counter_limit, 10, fb_cb, res_cb);
    Ferguson::sleepFor(TimeUnit(30));
    //In 30ms sleep we can do 2 to 4 counting
  }//Auto terminate upon destructing object
  EXPECT_GE(tested_fb, 2);
  EXPECT_LE(tested_fb, 4);
  // We are preempting in feedback, result shouldn't be executed
  EXPECT_EQ(tested_res, 0);
}

TEST(YieldingActionTest, ExecutionIterator) {
  const int counter_limit = 5;
  auto my_action          = MyYF::factory(counter_limit, 10);

  int i = 0;
  for(auto &fb : *my_action) {
    auto cfb = std::dynamic_pointer_cast<MFeedback const>(fb);
    EXPECT_EQ(cfb->i, i++);
  }
  auto res = as<MResult const>(my_action->waitResult());
  EXPECT_TRUE(res);
  EXPECT_NE(res.value(), MResultCSPtr(nullptr));
  EXPECT_EQ(res.value()->i, counter_limit + 1);
}

TEST(YieldingActionTest, ExecutionConstIterator) {
  const int counter_limit = 5;
  auto my_action          = MyYF::factory(counter_limit, 10);

  int i = 0;
  for(auto const &fb : *my_action) {
    auto cfb = std::dynamic_pointer_cast<MFeedback const>(fb);
    EXPECT_EQ(cfb->i, i++);
  }
  auto res = as<MResult const>(my_action->waitResult());
  EXPECT_TRUE(res);
  EXPECT_NE(res.value(), MResultCSPtr(nullptr));
  EXPECT_EQ(res.value()->i, counter_limit + 1);
}

TEST(YieldingActionTest, ExecutionStarOperator) {
  const int counter_limit = 5;
  auto my_action          = MyYF::factory(counter_limit, 10);
  auto res                = std::dynamic_pointer_cast<MResult const>(**my_action);
  EXPECT_NE(res, MResultCSPtr(nullptr));
  EXPECT_EQ(res->i, counter_limit + 1);
}

}// namespace YieldingAction
