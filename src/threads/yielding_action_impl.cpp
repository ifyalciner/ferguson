// MIT License
//
// Copyright (c) 2022 Ibrahim Faruk YALCINER
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

#include "yielding_action_impl.h"

/* 3rd Part*/
#ifdef DT_BOOST_COROUTINE2
#include <boost/coroutine2/all.hpp>
namespace coroutines = boost::coroutines2;
#endif

#ifdef DT_BOOST_COROUTINE
#include <boost/coroutine/all.hpp>
namespace coroutines = boost::coroutines;
#endif

namespace Ferguson::Threads {

YieldingActionImpl::YieldingActionImpl(ActionID parent,                                         //
                                       std::string const &name,                                 //
                                       std::string const &type,                                 //
                                       TimeUnit const &max_terminate_delay,                     //
                                       std::optional<ActionFeedbackCB> const &feedback_callback,//
                                       std::optional<ActionResultCB> const &result_callback)
    : ActionImpl(parent,             //
                 name,               //
                 type,               //
                 max_terminate_delay,//
                 feedback_callback,  //
                 result_callback) {}

void
YieldingActionImpl::run(Anytime::ThreadSPtr thread_sync) {
  using CoroType   = typename coroutines::coroutine<ActionFeedbackCSPtr const>;
  using CoroSource = typename CoroType::pull_type;
  using CoroSink   = typename CoroType::push_type;

  ActionResultCSPtr result;

  CoroSource coro([this, &result](CoroSink &sink) {
    result = run([&sink](ActionFeedbackCSPtr const &feedback) { sink(feedback); });
    return;
  });

  for(auto &feedback : coro) {
    if(thread_sync->isTerminateRequested()) {
      return;
    }
    // nullptr feedbacks are ignored.
    if(feedback) {
      thread_sync->publishFeedback(feedback);
      logFeedbackSent(feedback, __FILE__, __LINE__);
    }
  }
  // When done return result
  thread_sync->publishResult(result);
  logResultSent(result, __FILE__, __LINE__);
}

}// namespace Ferguson::Threads
