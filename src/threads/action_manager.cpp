// MIT License
//
// Copyright (c) 2022 Ibrahim Faruk YALCINER
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "action_manager.h"

#include <iostream>
#include <mutex>
#include <unordered_map>
#include <unordered_set>
#include <vector>


namespace Ferguson::Threads {

class ActionManagerImpl: public ActionManager {
public:
  ActionManagerImpl() noexcept
      : update(),         //
        actions(),        //
        parent_to_child(),//
        child_to_parent() {}

  virtual ~ActionManagerImpl() = default;

  void
  addAction(ActionImpl const *const action) noexcept final {
    try {
      std::unique_lock<std::mutex> lock(update);
      if(actions.find(action->id()) != actions.end()) {
        std::clog << "ActionID " << action->id() << " does not exist" << std::endl;
        return;
      }
      actions[action->id()] = action;
      parent_to_child[action->parent()].emplace(action->id());
      child_to_parent[action->id()] = action->parent();
    }
    catch(std::exception const &e) {
      std::clog << "addAction exception: " << e.what() << std::endl;
    }
  }

  void
  removeAction(ActionIDType action_id) noexcept final {
    try {
      std::unique_lock<std::mutex> lock(update);
      auto action_it = actions.find(action_id);
      if(action_it == actions.end()) {
        std::clog << "ActionID " << action_id << " not available" << std::endl;
        return;
      }
      actions.erase(action_it);
      parent_to_child.erase(child_to_parent[action_id]);
      child_to_parent.erase(action_id);
    }
    catch(std::exception const &e) {
      std::clog << "removeAction exception: " << e.what() << std::endl;
    }
  }

private:
  std::mutex update;
  std::unordered_map<ActionIDType, Threads::ActionImpl const *> actions;
  std::unordered_map<ActionIDType, std::unordered_set<ActionIDType>> parent_to_child;
  std::unordered_map<ActionIDType, ActionIDType> child_to_parent;
};

std::shared_ptr<ActionManager>
ActionManager::get() {
  static std::shared_ptr<ActionManager> am(std::make_shared<ActionManagerImpl>());
  return am;
}

} // namespace Ferguson::Threads

