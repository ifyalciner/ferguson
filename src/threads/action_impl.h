// MIT License
//
// Copyright (c) 2024 Ibrahim Faruk YALCINER
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef FERGUSON_SRC_THREADS_ACTION_IMPL_H
#define FERGUSON_SRC_THREADS_ACTION_IMPL_H

/* CPP */
#include <atomic>
#include <exception>
#include <unordered_map>

/* Boost */
#include <boost/thread.hpp>

/* Local */
#include "ferguson/synchronizers/anytime.h"
#include "ferguson/threads/action.h"
#include "ferguson/utils.h"

/* Utility Methods */
#define FERGUSON_LOG_MESSAGE(message, mode) logMessage(message, __FILE__, __LINE__, mode)
#define FERGUSON_LOG_FEEDBACK_RECEIVED(message, mode) logProductReceived(message, __FILE__, __LINE__, mode)
#define FERGUSON_LOG_RESULT_RECEIVED(message, mode) logResultReceived(message, __FILE__, __LINE__, mode)
#define GET_TYPE() boost::core::demangle(typeid(*this).name()).substr(6)

namespace Ferguson::Threads {

enum LogMode {
  INFO    = 0,
  WARNING = 1,
  ERROR   = 2,
  FATAL   = 3,
  DEBUG   = 4,
};

class ActionManager;
class ActionImpl: public virtual Action, public std::enable_shared_from_this<ActionImpl> {
  NOCOPYASSIGN(ActionImpl);

public:
  ActionImpl(ActionID parent,                    //
             TaskBaseSPtr task,                  //
             std::string name,                   //
             std::string task_class_name,        //
             TimeUnit const &max_terminate_delay,//
             std::optional<ActionProductCB> product_callback = std::nullopt);

  virtual ~ActionImpl();

  void
  preempt() final;

  bool
  finished() final;

  void waitUntilFinished(TimeUnit) final;

  std::any
  getProduct() final;

  void
  throwException() final;

  /**
   * @brief Return current action's ID.
   */
  uint64_t
  id() const final;

  /**
   * @brief Returns current action's parent's ID.
   */
  uint64_t
  parent() const final;

  std::shared_ptr<ActionImpl>
  getptr();

  std::shared_ptr<const ActionImpl>
  getcptr() const;

protected:
  //protected:
  //  /**
  //   * @brief
  //   * @param message
  //   * @param file_name
  //   * @param line_no
  //   * @param mode
  //   */
  //  void
  //  logMessage(std::string const &message,  //
  //             std::string const &file_name,//
  //             uint64_t line_no,            //
  //             uint64_t mode);
  //
  //  /**
  //   * @brief
  //   * @param message
  //   * @param file_name
  //   * @param line_no
  //   * @param mode
  //   */
  //  void
  //  logMessage(boost::property_tree::ptree const &message,//
  //             std::string const &file_name,              //
  //             uint64_t line_no,                          //
  //             uint64_t mode);
  //
  //  /**
  //   * @brief Utility method to report that a product is sent to parent.
  //   * @param product
  //   * @param file_name
  //   * @param line_no
  //   * @param mode
  //   */
  //  void
  //  logProductSent(ActionProductCSPtr const &product,//
  //                  std::string const &file_name,       //
  //                  uint64_t line_no);
  //
  //  /**
  //   * @brief Utility method to report that a result is sent to parent.
  //   * @param result
  //   * @param file_name
  //   * @param line_no
  //   */
  //  void
  //  logResultSent(ActionResultCSPtr const &result,//
  //                std::string const &file_name,   //
  //                uint64_t line_no);
  //  /**
  //   * @brief Utility method to report that a product is received from a child action.
  //   * @param product
  //   * @param file_name
  //   * @param line_no
  //   * @param mode
  //   */
  //  void
  //  logProductReceived(ActionProductCSPtr const &product,//
  //                      uint64_t from,                      //
  //                      std::string const &file_name,       //
  //                      uint64_t line_no);
  //
  //  /**
  //   * @brief Utility method to report that a result is received from a child action.
  //   * @param result
  //   * @param from
  //   * @param file_name
  //   * @param line_no
  //   */
  //  void
  //  logResultReceived(ActionResultCSPtr const &result,//
  //                    uint64_t from,                  //
  //                    std::string const &file_name,   //
  //                    uint64_t line_no);
  //
  //  /**
  //   * @brief Default implementation to print out user logs. Meant to be overridden by derived classes.
  //   * @param pt
  //   * @param mode
  //   */
  //  virtual void
  //  logUser(boost::property_tree::ptree const &pt,//
  //          uint64_t mode);
  //
  //private:
  //  /**
  //   * @brief
  //   * @param file_name
  //   * @param line_no
  //   * @param mode
  //   */
  //  boost::property_tree::ptree
  //  getHeader(std::string const &file_name,//
  //            uint64_t line_no);
  //
  //  /**
  //   * @brief
  //   * @param file_name
  //   * @param line_no
  //   * @param mode
  //   */
  //  void
  //  logCreated(std::string const &file_name,//
  //             uint64_t line_no);
  //
  //  /**
  //   * @brief
  //   * @param file_name
  //   * @param line_no
  //   */
  //  void
  //  logDestroyed(std::string const &file_name,//
  //               uint64_t line_no);
  //
private:
  void
  executeRun() noexcept;

  void
  callback_handler() noexcept;

private:
  TaskBaseSPtr const _task;
  uint64_t const _id;
  uint64_t const _parent;
  std::string const _name;
  std::string const _task_class_name;
  std::optional<ActionProductCB> const _product_callback;
  std::shared_ptr<ActionManager> _manager;
  Synchronizer::AnytimeHostSPtr _host_sync;
  Synchronizer::AnytimeThreadSPtr _thread_sync;
  // TaskBase thread handles the running the task
  std::shared_ptr<boost::thread> _task_thread_handle_sptr;
  // Host thread handles the callback functions. This makes sure that user defined callbacks cannot hold the thread.
  std::atomic_bool _host_thread_running;
  std::shared_ptr<boost::thread> _host_thread_handle_sptr;
};

}// namespace Ferguson::Threads

#endif//FERGUSON_SRC_THREADS_ACTION_IMPL_H
