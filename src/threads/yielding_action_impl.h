// MIT License
//
// Copyright (c) 2022 Ibrahim Faruk YALCINER
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

#ifndef FERGUSON_SRC_THREADS_YIELDING_ACTION_H
#define FERGUSON_SRC_THREADS_YIELDING_ACTION_H

#include "action_impl.h"

namespace Ferguson::Threads {

class YieldingActionImpl: public ActionImpl {
  NOCOPYASSIGN(YieldingActionImpl);

public:
  YieldingActionImpl(ActionID parent,                                                        //
                     std::string const &name,                                                //
                     std::string const &type,                                                //
                     TimeUnit const &max_terminate_delay,                                    //
                     std::optional<ActionFeedbackCB> const &feedback_callback = std::nullopt,//
                     std::optional<ActionResultCB> const &result_callback     = std::nullopt);

  ~YieldingActionImpl() override = default;

protected:
  /**
   * @brief Abstract method to be overriden by custom action. This method will be automatically called by thread and
   *        the value returned by it will be delivered to action owner. Feedbacks should be published through provided
   *        function argument.
   * @param publish_feedback Method to publish feedback and check if terminate is called. This method may not return
   *                         if terminate is called. `nullptr` feedbacks will not be published so can be used only to
   *                         check if terminate is called.
   * @return Action result constant pointer.
   */
  virtual ActionResultCSPtr
  run(ActionFeedbackCB const &publish_feedback) = 0;

private:
  // Set inherited logFeedbackSent method private
  using ActionImpl::logResultSent;

  void
  run(Anytime::ThreadSPtr thread_sync) final;
};

}// namespace Ferguson::Threads

#endif//FERGUSON_SRC_THREADS_YIELDING_ACTION_H
