// MIT License
//
// Copyright (c) 2022 Ibrahim Faruk YALCINER
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include <ferguson/state/reverting_bool.h>
#include <mutex>

namespace Ferguson::State {

class RevertingBoolImpl: public RevertingBool {
  NOCOPYASSIGN(RevertingBoolImpl);

public:
  explicit RevertingBoolImpl(bool default_value,                //
                             Ferguson::TimeUnit reverting_delay,//
                             Ferguson::TimeUnit activation_delay = Ferguson::TimeUnit(0))
      : _default_value(default_value),                 //
        _reverting_delay(reverting_delay),             //
        _activation_delay(activation_delay),           //
        _non_default_start(Ferguson::TimePoint::max()),//
        _non_default_end(Ferguson::TimePoint::max()) {}
  virtual ~RevertingBoolImpl() = default;

  RevertingBoolImpl &
  operator=(bool x) override {
    auto current_time = Ferguson::Clock::now();
    std::unique_lock<std::mutex> lk(_m);
    if(x != _default_value) {
      // Get earliest deadline
      _non_default_start = std::min(_non_default_start, Ferguson::toDeadline(_activation_delay, current_time));
      // Get the latest deadline. New assignment will push the deadline further
      _non_default_end = Ferguson::toDeadline(_reverting_delay, current_time);
    }
    else {
      _non_default_start = std::max(_non_default_start, current_time);
      _non_default_end   = Ferguson::toDeadline(_activation_delay, current_time);
    }
    return *this;
  }

  operator bool() const override {
    auto current_time = Ferguson::Clock::now();
    {// Update values
      std::unique_lock<std::mutex> lk(_m);
      if(_non_default_end <= current_time) {
        // Push a ghost deadline in eternity
        _non_default_start = Ferguson::TimePoint::max();
        _non_default_end   = Ferguson::TimePoint::max();
      }
    }
    return value_at(current_time);
  }

  Ferguson::TimePoint
  revertingDeadline() const override {
    std::unique_lock<std::mutex> lk(_m);
    return _non_default_end;
  }

private:
  bool
  value_at(Ferguson::TimePoint const &current_time) const {
    if(_non_default_end < current_time || current_time < _non_default_start) {
      return _default_value;
    }
    else {
      return !_default_value;
    }
  }

  bool const _default_value;
  Ferguson::TimeUnit const _reverting_delay;
  Ferguson::TimeUnit const _activation_delay;

  mutable Ferguson::TimePoint _non_default_start;
  mutable Ferguson::TimePoint _non_default_end;
  mutable std::mutex _m;
};

std::shared_ptr<RevertingBool>
RevertingBool::factory(bool default_value,                //
                       Ferguson::TimeUnit reverting_delay,//
                       Ferguson::TimeUnit activation_delay) {
  return std::static_pointer_cast<RevertingBool>(
      std::make_shared<RevertingBoolImpl>(default_value, reverting_delay, activation_delay));
}

}// namespace Ferguson::State
