// MIT License
//
// Copyright (c) 2022 Ibrahim Faruk YALCINER
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include <ferguson/state/delayed_bool.h>
#include <mutex>

namespace Ferguson::State {

class DelayedBoolImpl: public DelayedBool {
  NOCOPYASSIGN(DelayedBoolImpl);

public:
  DelayedBoolImpl(bool initial,
                  Ferguson::TimeUnit true_delay,//
                  Ferguson::TimeUnit false_delay)
      : _true_delay(true_delay),               //
        _false_delay(false_delay),             //
        _state(initial),                       //
        _state_changed(Ferguson::Clock::now()),//
        _delayed_state(false),                 // doesn't matter
        _new_state_requested() {}

  virtual ~DelayedBoolImpl() = default;

  DelayedBoolImpl &
  operator=(bool x) override {
    setState(x);
    return *this;
  }

  operator bool() const override { return getState().first; }

  bool
  setImmediately(bool x) override {
    _state               = x;
    _state_changed       = Ferguson::Clock::now();
    _delayed_state       = x;
    _new_state_requested = TimePoint();
    return _state;
  }

  std::pair<bool, Ferguson::TimePoint>
  getState() const {
    std::unique_lock<std::mutex> lk(_m);
    update();
    return std::make_pair(bool(_state), _state_changed);
  }

  bool
  setState(bool x, Ferguson::TimePoint changed = TimePoint()) {
    if(changed == TimePoint()) {
      changed = Ferguson::Clock::now();
    }
    std::unique_lock<std::mutex> lk(_m);
    if(changed < _new_state_requested) {
      throw std::invalid_argument("Changed can't be earlier than previous setting time.");
    }
    if(_new_state_requested < changed) {
      update();
    }
    if(_new_state_requested == TimePoint() || x != _delayed_state) {
      _new_state_requested = changed;
      _delayed_state       = x;
    }
    else if(x == _state) {
      _new_state_requested = TimePoint();
    }
    update();
    return _state;
  }

private:
  void
  update() const {
    if(_new_state_requested != TimePoint()) {
      auto dl = toDeadline(_delayed_state ? _true_delay : _false_delay, _new_state_requested);
      if(Ferguson::Clock::now() >= dl) {
        _state               = _delayed_state;
        _state_changed       = dl;
        _new_state_requested = TimePoint();
      }
    }
  }

  Ferguson::TimeUnit const _true_delay;
  Ferguson::TimeUnit const _false_delay;

  mutable bool _state;
  mutable Ferguson::TimePoint _state_changed;
  mutable bool _delayed_state;
  mutable Ferguson::TimePoint _new_state_requested;
  mutable std::mutex _m;
};

class DelayedBoolJitterImpl: public DelayedBool {
  NOCOPYASSIGN(DelayedBoolJitterImpl)

public:
  DelayedBoolJitterImpl(bool initial,
                        Ferguson::TimeUnit true_delay, //
                        Ferguson::TimeUnit false_delay,//
                        Ferguson::TimeUnit activation_delay)
      : main(initial, true_delay - activation_delay, false_delay - activation_delay),//
        activation_delay(initial, activation_delay, activation_delay)                //

  {
    if(activation_delay > true_delay || activation_delay > false_delay) {
      throw std::invalid_argument("Activation delay cannot exceed to true/false delays!");
    }
  }

  virtual ~DelayedBoolJitterImpl() = default;

  DelayedBoolJitterImpl &
  operator=(bool x) override {
    activation_delay = x;
    auto s           = activation_delay.getState();
    main.setState(s.first, s.second);
    return *this;
  }
  operator bool() const override {
    auto s = activation_delay.getState();
    return main.setState(s.first, s.second);
  }

  bool
  setImmediately(bool x) override {
    activation_delay.setImmediately(x);
    main.setImmediately(x);
    return main;
  }

  mutable DelayedBoolImpl main;
  mutable DelayedBoolImpl activation_delay;
};

std::shared_ptr<DelayedBool>
DelayedBool::factory(bool initial,                  //
                     Ferguson::TimeUnit true_delay, //
                     Ferguson::TimeUnit false_delay,//
                     Ferguson::TimeUnit activation_delay) {
  if(activation_delay == TimeUnit(0)) {
    return std::static_pointer_cast<DelayedBool>(std::make_shared<DelayedBoolImpl>(initial, true_delay, false_delay));
  }
  else {
    return std::static_pointer_cast<DelayedBool>(
        std::make_shared<DelayedBoolJitterImpl>(initial, true_delay, false_delay, activation_delay));
  }
}
}// namespace Ferguson::State
