// MIT License
//
// Copyright (c) 2024 Ibrahim Faruk YALCINER
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef INTERRUPTABLE_IMPL_H_
#define INTERRUPTABLE_IMPL_H_

/* Local */
#include "ferguson/synchronizers/interruptable.h"
#include "synchronizer_impl.h"

/* C++ */
#include <atomic>

namespace Ferguson::Synchronizer {

class InterruptableImpl: public SynchronizerImpl,         //
                         public virtual InterruptableHost,//
                         public virtual InterruptableThread {
  NOCOPYASSIGN(InterruptableImpl);

public:
  explicit InterruptableImpl(TimeUnit response_time);
  ~InterruptableImpl() override;

  /*---------------------------------------------------------------*/
  /* Host Methods */

  /**
   * @sa Host::hostSafeExecute
   */
  bool
  hostSafeExecute(std::function<void()> const &procedure,//
                  TimePoint deadline,                    //
                  const NullaryBoolF &fail_if_true,      //
                  const NullaryBoolF &delay_until_true) override;

  /**
   * @sa Host::hostWaitUntil
   */
  bool
  hostWaitUntil(TimePoint deadline,              //
                const NullaryBoolF &fail_if_true,//
                const NullaryBoolF &delay_until_true) override;

  /**
   * @sa Host::requestInitialize
   */
  void
  requestInitialize() final;

  /**
   * @sa Host::requestTerminate
   */
  void
  requestTerminate() final;

  /**
   * @sa Host::isWaitingInitialization
   */
  bool
  isWaitingTermination() final;

  /**
   * @sa Host::isInitialized
   */
  bool
  isInitialized() final;

  /**
   * @sa Host::isTerminated
   */
  bool
  waitInitialized(TimeUnit timeout) final;

  /**
   * @sa Host::isTerminated
   * @details This method should be checked by child methods in a period less then allowed \b response_time
   * @warning This method should only be used by child classes for proper timing check
   */
  bool
  isTerminated() final;

  /**
   * @sa Host::waitTerminated
   */
  bool
  waitTerminated(TimeUnit timeout) final;

  /**
   * @sa Host::threadException
   */
  std::exception_ptr
  threadException() final;

  /*---------------------------------------------------------------*/
  /* Thread Methods */

  /**
   * @sa Thread::threadSafeExecute
   */
  bool
  threadSafeExecute(std::function<void()> const &procedure,//
                    TimePoint deadline,                    //
                    const NullaryBoolF &fail_if_true,      //
                    const NullaryBoolF &delay_until_true) override;

  /**
   * @sa Thread::threadWaitUntil
   */
  bool
  threadWaitUntil(TimePoint deadline,              //
                  const NullaryBoolF &fail_if_true,//
                  const NullaryBoolF &delay_until_true) override;

  /**
   * @sa Thread::isInitializeRequested
   */
  bool
  isInitializeRequested() final;

  /**
   * @sa Thread::isTerminateRequested
   */
  bool
  isTerminateRequested() final;

  /**
   * @sa Thread::waitInitializationRequest
   */
  bool
  waitInitializationRequest() final;

  /**
   * @sa Thread::setInitialized
   */
  void
  setInitialized() final;

  /**
   * @sa Thread::setTerminated
   */
  void
  setTerminated() noexcept final;

  /**
   * @sa Thread::sleepUntil
   */
  void
  sleepUntil(TimePoint deadline) final;

  /**
   * @sa Thread::sleepFor
   */
  void
  sleepFor(TimeUnit const &duration) final;

  /**
   * @sa Thread::throwException
   */
  void
  throwException(std::exception_ptr exception) final;

protected:
  std::atomic_bool _initialize_requested;
  std::atomic_bool _initialized;
  std::atomic_bool _terminate_requested;
  std::atomic_bool _terminated;
  std::exception_ptr _exception_ptr;
  std::atomic<::Ferguson::TimePoint> _last_terminate_check;
};

}// namespace Ferguson::Synchronizer

#endif// INTERRUPTABLE_IMPL_H_
