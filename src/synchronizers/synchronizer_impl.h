// MIT License
//
// Copyright (c) 2024 Ibrahim Faruk YALCINER
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef SYNCHRONIZER_IMPL_H
#define SYNCHRONIZER_IMPL_H

/* Boost */
#include <boost/thread/condition_variable.hpp>

/* Ferguson */
#include "ferguson/synchronizers/synchronizer.h"

namespace Ferguson::Synchronizer {
/**
 * @brief SynchronizerImplementation class for Synchronizer
 * @sa Synchronizer
 */
class SynchronizerImpl: public virtual Synchronizer {
  NOCOPYASSIGN(SynchronizerImpl);

public:
  explicit SynchronizerImpl(TimeUnit response_time);

  virtual ~SynchronizerImpl() = default;

  bool
  safeExecute(std::function<void()> const &procedure,//
              TimePoint deadline,                    //
              const NullaryBoolF &fail_if_true,      //
              const NullaryBoolF &delay_until_true) override;

  bool
  waitUntil(TimePoint deadline,              //
            const NullaryBoolF &fail_if_true,//
            const NullaryBoolF &delay_until_true) override;

  std::pair<bool, LockType>
  acquireLock(NullaryBoolF const &fail_if_true = ALWAYS_FALSE);

  /**
   * @brief Update the synchronizer
   * @note This method is called by the thread owner to signal the synchronizer to update.
   * @warning This method should not be overriden without making sure that all children classes are aware of the change.
   */
  void
  update() noexcept final;

  /**
   * @brief Utility raising error if given deadline is exceeded upon execution
   * @param procedure
   * @param deadline
   */
  void
  timeProcedure(std::function<void()> const &procedure,//
                TimePoint deadline = TimePoint::max());

protected:
  /* Every method in this class will return at most in _response_time upon fail_if_true returns true. */
  TimeUnit const _response_time;
  /* Mutex that all methods are waiting on */
  MutexType _update_mutex;
  /* CV to signal checks from other threads */
  boost::condition_variable_any _update;
};

}// namespace Ferguson::Synchronizer

#endif//SYNCHRONIZER_IMPL_H
