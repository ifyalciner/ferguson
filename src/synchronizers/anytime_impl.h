// MIT License
//
// Copyright (c) 2022 Ibrahim Faruk YALCINER
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

#ifndef FERGUSON_SRC_TREAD_ACTION_H
#define FERGUSON_SRC_TREAD_ACTION_H

/* CPP */
#include <queue>

/* Local */
#include "interruptable_impl.h"
#include <ferguson/synchronizers/anytime.h>

namespace Ferguson::Synchronizer {

class AnytimeImpl: public InterruptableImpl,  //
                   public virtual AnytimeHost,//
                   public virtual AnytimeThread {
public:
  explicit AnytimeImpl(TimeUnit const &max_terminate_delay);

  /*---------------------------------------------------------------*/
  /* Host methods */

  /** @brief Get feedback from the function. This method blocks until mutex is available.
   *  @param deadline The deadline for the operation.
   *  @return The feedback if available, otherwise std::nullopt.
   */
  std::any
  getProduct(TimePoint deadline) final;

  /*---------------------------------------------------------------*/
  /* Thread methods */

  /** @brief Publish feedback to the function.
   *  @param feedback The feedback to be published.
   */
  void
  publishProduct(std::any feedback_ptr) final;

private:
  /** Following are used only if callbacks are not provided by the user. */
  std::queue<std::any> _product_queue;
};
}// namespace Ferguson::Synchronizer

#endif//FERGUSON_SRC_TREAD_ACTION_H
